﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class quitScript : MonoBehaviour
{
    private Button touch;

    // Start is called before the first frame update
    void Start()
    {
        touch = GetComponent<Button>();
        touch.onClick.AddListener(Detect);
    }

    private void Detect()
    {
        Application.Quit();
    }
}
