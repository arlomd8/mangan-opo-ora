﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class moveToHow : MonoBehaviour
{
    private Button touch;

    // Start is called before the first frame update
    void Start()
    {
        touch = GetComponent<Button>();
        touch.onClick.AddListener(Detect);
    }

    private void Detect()
    {
        SceneManager.LoadScene("HowToScene");
    }
}
