﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rondeMerah : MonoBehaviour
{
    public bgScriptEv final;

    public GameObject R01;
    public GameObject R02;
    public GameObject R03;
    public GameObject R04;
    public GameObject R05;
    public GameObject R06;
    public GameObject R07;
    public GameObject R08;

    public int scoreMerah = 0;
    public bool win = false;

    // Update is called once per frame
    void Update()
    {
        if (final.ronde == 1 && win == true)
        {
            R01.SetActive(true);
            scoreMerah++;
            win = false;
        }
        if (final.ronde == 2 && win == true)
        {
            R02.SetActive(true);
            scoreMerah++;
            win = false;
        }
        if (final.ronde == 3 && win == true)
        {
            R03.SetActive(true);
            scoreMerah++;
            win = false;
        }
        if (final.ronde == 4 && win == true)
        {
            R04.SetActive(true);
            scoreMerah++;
            win = false;
        }
        if (final.ronde == 5 && win == true)
        {
            R05.SetActive(true);
            scoreMerah++;
            win = false;
        }
        if (final.ronde == 6 && win == true)
        {
            R06.SetActive(true);
            scoreMerah++;
            win = false;
        }
        if (final.ronde == 7 && win == true)
        {
            R07.SetActive(true);
            scoreMerah++;
            win = false;
        }
        if (final.ronde == 8 && win == true)
        {
            R08.SetActive(true);
            scoreMerah++;
            win = false;
        }
    }
}
