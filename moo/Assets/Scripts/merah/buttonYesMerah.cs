﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonYesMerah : MonoBehaviour
{
    private Button yes;
    public bgScriptEv bgEv;
    public bgScriptRed bgRed;
    public GameObject panel;

    // Update is called once per frame
    void Start()
    {
        yes = GetComponent<Button>();
        yes.onClick.AddListener(Yes);
    }

    public void Yes()
    {
        panel.SetActive(false);
        bgEv.temp = 0;
        bgEv.finalMerah = 1;
        bgEv.indexMerah = bgRed.level;

        Debug.Log("yes");
    }
}
