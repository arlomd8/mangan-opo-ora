﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonNoMerah : MonoBehaviour
{
    private Button no;
    public bgScriptEv bgEv;
    public bgScriptRed bgRed;
    public GameObject panel;

    // Update is called once per frame
    void Start()
    {
        no = GetComponent<Button>();
        no.onClick.AddListener(No);
    }

    public void No()
    {
        panel.SetActive(false);
        bgEv.temp = 1;
        bgEv.indexMerah = 0;
        bgRed.level = 0;

        Debug.Log("no");
    }
}
