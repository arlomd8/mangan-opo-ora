﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class fadeIn : MonoBehaviour
{
    public Text copyright;
    public Transform scroll;
    public Transform panel;
    public bool condition = false;

    void Start()
    {
        copyright.canvasRenderer.SetAlpha(0.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (scroll.position.y == panel.position.y)
        {
            condition = true;
            startFadeIn();
        }
    }

    void startFadeIn()
    {
        copyright.CrossFadeAlpha(1, 2, false);
    }
}
