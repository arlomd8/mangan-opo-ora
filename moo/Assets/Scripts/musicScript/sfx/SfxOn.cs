﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SfxOn : MonoBehaviour
{
    public carryVar bg;
    private Button touch;

    // Start is called before the first frame update
    void Start()
    {
        touch = GetComponent<Button>();
        touch.onClick.AddListener(detect);
    }

    void detect()
    {
        bg.sfx = 1;
    }
}
