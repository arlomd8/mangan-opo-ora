﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menuBGM : MonoBehaviour
{
    public GameObject BGM;
    public receiveVar bg;

    // Update is called once per frame
    void Update()
    {
        if (bg.recv == 1)
        {
            BGM.SetActive(true);
        }
        if (bg.recv == 0)
        {
            BGM.SetActive(false);
        }
    }
}
