﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MusicOff : MonoBehaviour
{
    public carryVar bg;
    private Button touch;

    // Start is called before the first frame update
    void Start()
    {
        touch = GetComponent<Button>();
        touch.onClick.AddListener(detect);
    }

    void detect()
    {
        bg.carry = 0;
    }
}
