﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bgButton : MonoBehaviour
{
    public GameObject musicOff;
    public GameObject musicOn;
    public receiveVar bg;

    // Update is called once per frame
    void Update()
    {
        if (bg.recv == 0)
        {
            musicOn.SetActive(true);
            musicOff.SetActive(false);
        }
        else if (bg.recv == 1)
        {
            musicOn.SetActive(false);
            musicOff.SetActive(true);
        }
    }
}
