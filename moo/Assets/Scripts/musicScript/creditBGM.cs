﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class creditBGM : MonoBehaviour
{
    public receiveVar var;
    public GameObject BGM;
    public bool nyala = true;

    // Update is called once per frame
    void Update()
    {
        if (var.recv == 1)
        {
            BGM.SetActive(true);
            nyala = true;
        }
        if (var.recv == 0)
        {
            BGM.SetActive(false);
            nyala = false;
        }
        
    }
}
