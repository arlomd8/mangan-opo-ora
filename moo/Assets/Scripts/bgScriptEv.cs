﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bgScriptEv : MonoBehaviour
{
    public receiveVar var;
    
    public bgScriptBlue scriptBiru;
    public bgScriptRed scriptMerah;

    public rondeMerah rondM;
    public rondeBiru rondB;

    public GameObject turnBiruEng;
    public GameObject turnBiruIdn;
    public GameObject turnBiruJvn;
    public GameObject turnMerahEng;
    public GameObject turnMerahIdn;
    public GameObject turnMerahJvn;

    public GameObject closeMerah;
    public GameObject closeBiru;

    public int final = 0;
    public int finalBiru = 0;
    public int finalMerah = 0;

    public int ronde = 0;
    public int temp = 0;
    public int indexBiru = 0;
    public int indexMerah = 0;

    public GameObject B01;
    public GameObject B02;
    public GameObject B03;
    public GameObject B04;
    public GameObject B05;
    public GameObject B06;
    public GameObject B07;
    public GameObject B08;

    public GameObject H01;
    public GameObject H02;
    public GameObject H03;
    public GameObject H04;
    public GameObject H05;
    public GameObject H06;
    public GameObject H07;
    public GameObject H08;

    // Update is called once per frame
    void Update()
    {
        repeated();
        round();
    }

    void repeated()
    {
        //merah-biru turn
        if (temp == 0)
        {
            temp0();
            //Invoke("temp0", 3);
        }
        if (temp == 1)
        {
            temp1();
            //Invoke("temp1", 3);
        }
    }

    void temp0()
    {
        if (var.language == 1)
        {
            turnBiruEng.SetActive(true);
            turnBiruIdn.SetActive(false);
            turnBiruJvn.SetActive(false);
            turnMerahEng.SetActive(false);
            turnMerahIdn.SetActive(false);
            turnMerahJvn.SetActive(false);
        }
        else if (var.language == 2)
        {
            turnBiruEng.SetActive(false);
            turnBiruIdn.SetActive(true);
            turnBiruJvn.SetActive(false);
            turnMerahEng.SetActive(false);
            turnMerahIdn.SetActive(false);
            turnMerahJvn.SetActive(false);
        }
        else if (var.language == 3)
        {
            turnBiruEng.SetActive(false);
            turnBiruIdn.SetActive(false);
            turnBiruJvn.SetActive(true);
            turnMerahEng.SetActive(false);
            turnMerahIdn.SetActive(false);
            turnMerahJvn.SetActive(false);
        }
        closeBiru.SetActive(false);
        closeMerah.SetActive(true);
    }

    void temp1()
    {
        if (var.language == 1)
        {
            turnBiruEng.SetActive(false);
            turnBiruIdn.SetActive(false);
            turnBiruJvn.SetActive(false);
            turnMerahEng.SetActive(true);
            turnMerahIdn.SetActive(false);
            turnMerahJvn.SetActive(false);
        }
        else if (var.language == 2)
        {
            turnBiruEng.SetActive(false);
            turnBiruIdn.SetActive(false);
            turnBiruJvn.SetActive(false);
            turnMerahEng.SetActive(false);
            turnMerahIdn.SetActive(true);
            turnMerahJvn.SetActive(false);
        }
        else if (var.language == 3)
        {
            turnBiruEng.SetActive(false);
            turnBiruIdn.SetActive(false);
            turnBiruJvn.SetActive(false);
            turnMerahEng.SetActive(false);
            turnMerahIdn.SetActive(false);
            turnMerahJvn.SetActive(true);
        }
        closeBiru.SetActive(true);
        closeMerah.SetActive(false);
    }

    void round()
    {
        final = finalBiru + finalMerah;

        //kondisi turn
        while (final == 2)
        {
            ronde++;
            if (indexBiru == 8 && indexMerah == 1)
            {
                hiddenBiru();
                hiddenMerah();

                temp = 1;
                indexBiru = 0;
                indexMerah = 0;
                finalBiru = 0;
                finalMerah = 0;

                rondM.win = true;
                final = 0;
            }
            else if (indexBiru == 1 && indexMerah == 8)
            {
                hiddenBiru();
                hiddenMerah();

                temp = 0;
                indexBiru = 0;
                indexMerah = 0;
                finalBiru = 0;
                finalMerah = 0;

                rondB.win = true;
                final = 0;
            }
            else if (indexBiru < indexMerah)
            {
                hiddenBiru();
                hiddenMerah();

                temp = 1;
                indexBiru = 0;
                indexMerah = 0;
                finalBiru = 0;
                finalMerah = 0;

                rondM.win = true;
                final = 0;
            }
            else if (indexBiru > indexMerah)
            {
                hiddenBiru();
                hiddenMerah();

                temp = 0;
                indexBiru = 0;
                indexMerah = 0;
                finalBiru = 0;
                finalMerah = 0;

                rondB.win = true;
                final = 0;
            }
            else if (indexBiru == indexMerah)
            {
                hiddenBiru();
                hiddenMerah();

                if (temp == 0)
                {
                    temp = 1;
                }
                else if (temp == 1)
                {
                    temp = 0;
                }

                indexBiru = 0;
                indexMerah = 0;
                finalBiru = 0;
                finalMerah = 0;

                rondM.win = true;
                rondB.win = true;
                final = 0;
            }
        }
    }

    void hiddenBiru()
    {
        if (indexBiru == 1 && final == 2)
        {
            B01.SetActive(false);
        }
        else if (indexBiru == 2 && final == 2)
        {
            B02.SetActive(false);
        }
        else if (indexBiru == 3 && final == 2)
        {
            B03.SetActive(false);
        }
        else if (indexBiru == 4 && final == 2)
        {
            B04.SetActive(false);
        }
        else if (indexBiru == 5 && final == 2)
        {
            B05.SetActive(false);
        }
        else if (indexBiru == 6 && final == 2)
        {
            B06.SetActive(false);
        }
        else if (indexBiru == 7 && final == 2)
        {
            B07.SetActive(false);
        }
        else if (indexBiru == 8 && final == 2)
        {
            B08.SetActive(false);
        }
    }

    void hiddenMerah()
    {
        if (indexMerah == 1 && final == 2)
        {
            H01.SetActive(false);
        }
        else if (indexMerah == 2 && final == 2)
        {
            H02.SetActive(false);
        }
        else if (indexMerah == 3 && final == 2)
        {
            H03.SetActive(false);
        }
        else if (indexMerah == 4 && final == 2)
        {
            H04.SetActive(false);
        }
        else if (indexMerah == 5 && final == 2)
        {
            H05.SetActive(false);
        }
        else if (indexMerah == 6 && final == 2)
        {
            H06.SetActive(false);
        }
        else if (indexMerah == 7 && final == 2)
        {
            H07.SetActive(false);
        }
        else if (indexMerah == 8 && final == 2)
        {
            H08.SetActive(false);
        }
    }
}
