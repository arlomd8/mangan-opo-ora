﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class touchBeruangBiru : MonoBehaviour
{
    private Button hewan;
    public bgScriptBlue final;
    public receiveVar bg;
    public GameObject panelEng;
    public GameObject panelIdn;
    public GameObject panelJvn;

    // Start is called before the first frame update
    public void Start()
    {
        hewan = GetComponent<Button>();
        hewan.onClick.AddListener(beruang);
    }
    
    public void beruang()
    {
        final.level = 7;
        final.index = 1;
        if (bg.language == 1)
        {
            panelEng.SetActive(true);
            panelIdn.SetActive(false);
            panelJvn.SetActive(false);
        }
        else if (bg.language == 2)
        {
            panelEng.SetActive(false);
            panelIdn.SetActive(true);
            panelJvn.SetActive(false);
        }
        else if (bg.language == 3)
        {
            panelEng.SetActive(false);
            panelIdn.SetActive(false);
            panelJvn.SetActive(true);
        }
    }
}
