﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonNoBiru : MonoBehaviour
{
    private Button no;
    public bgScriptBlue bgBlue;
    public bgScriptEv bgEv;
    public GameObject panel;

    // Update is called once per frame
    void Start()
    {
        no = GetComponent<Button>();
        no.onClick.AddListener(No);
    }

    public void No()
    {
        panel.SetActive(false);
        bgEv.temp = 0;
        bgEv.indexBiru = 0;
        bgBlue.level = 0;

        Debug.Log("no");
    }
}
