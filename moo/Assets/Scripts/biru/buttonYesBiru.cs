﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonYesBiru : MonoBehaviour
{
    private Button yes;
    public bgScriptBlue bgBlue;
    public bgScriptEv bgEv;
    public GameObject panel;

    // Update is called once per frame
    void Start()
    {
        yes = GetComponent<Button>();
        yes.onClick.AddListener(Yes);
    }

    public void Yes()
    {
        panel.SetActive(false);
        bgEv.temp = 1;
        bgEv.finalBiru = 1;
        bgEv.indexBiru = bgBlue.level;

        Debug.Log("yes");
    }
}
