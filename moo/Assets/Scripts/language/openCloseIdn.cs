﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class openCloseIdn : MonoBehaviour
{
    private Button open;
    public carryVar var;

    // Start is called before the first frame update
    void Start()
    {
        open = GetComponent<Button>();
        open.onClick.AddListener(Detect);
    }

    private void Detect()
    {
        var.lang = 2;
    }
}
