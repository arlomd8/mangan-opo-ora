﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class forLanguage : MonoBehaviour
{
    public int language = 1;
    public carryVar var;
    public GameObject settingEng;
    public GameObject gameEng;
    public GameObject instrutEng;
    public GameObject langEng;
    public GameObject chainEng;

    public GameObject settingIdn;
    public GameObject gameIdn;
    public GameObject instrutIdn;
    public GameObject langIdn;
    public GameObject chainIdn;

    public GameObject settingJvn;
    public GameObject gameJvn;
    public GameObject instrutJvn;
    public GameObject langJvn;
    public GameObject chainJvn;

    // Update is called once per frame
    void Update()
    {
        language = var.lang;
        if (var.lang == 1)
        {
            settingEng.SetActive(true);
            gameEng.SetActive(true);
            instrutEng.SetActive(true);
            langEng.SetActive(true);
            chainEng.SetActive(true);

            settingIdn.SetActive(false);
            gameIdn.SetActive(false);
            instrutIdn.SetActive(false);
            langIdn.SetActive(false);
            chainIdn.SetActive(false);

            settingJvn.SetActive(false);
            gameJvn.SetActive(false);
            instrutJvn.SetActive(false);
            langJvn.SetActive(false);
            chainJvn.SetActive(false);
        }    
        else if(var.lang == 2)
        {
            settingEng.SetActive(false);
            gameEng.SetActive(false);
            instrutEng.SetActive(false);
            langEng.SetActive(false);
            chainEng.SetActive(false);

            settingIdn.SetActive(true);
            gameIdn.SetActive(true);
            instrutIdn.SetActive(true);
            langIdn.SetActive(true);
            chainIdn.SetActive(true);

            settingJvn.SetActive(false);
            gameJvn.SetActive(false);
            instrutJvn.SetActive(false);
            langJvn.SetActive(false);
            chainJvn.SetActive(false);
        }
        else if (var.lang == 3)
        {
            settingEng.SetActive(false);
            gameEng.SetActive(false);
            instrutEng.SetActive(false);
            langEng.SetActive(false);
            chainEng.SetActive(false);

            settingIdn.SetActive(false);
            gameIdn.SetActive(false);
            instrutIdn.SetActive(false);
            langIdn.SetActive(false);
            chainIdn.SetActive(false);

            settingJvn.SetActive(true);
            gameJvn.SetActive(true);
            instrutJvn.SetActive(true);
            langJvn.SetActive(true);
            chainJvn.SetActive(true);
        }
    }
}
