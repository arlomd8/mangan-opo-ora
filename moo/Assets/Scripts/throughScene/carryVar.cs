﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carryVar : MonoBehaviour
{
    public scriptBetween bg;
    public int carry = 1;
    public int lang = 1;
    public int sfx = 1;

    // Update is called once per frame
    void Update()
    {
        if (carry == 1)
        {
            bg.musicNyala = true;
        }
        else if (carry == 0)
        {
            bg.musicNyala = false;
        }

        PlayerPrefs.SetInt("SoundCarry", carry);
        PlayerPrefs.SetInt("LanguageCarry", lang);
        PlayerPrefs.SetInt("sfxCarry", sfx);
    }
}
