﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class openPanel : MonoBehaviour
{
    private Button open;
    public GameObject panelSetting;

    // Start is called before the first frame update
    void Start()
    {
        open = GetComponent<Button>();
        open.onClick.AddListener(Detect);
    }

    private void Detect()
    {
        panelSetting.SetActive(true);
    }
}
