﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkWinner : MonoBehaviour
{
    public GameObject redWinEng;
    public GameObject redWinIdn;
    public GameObject redWinJvn;

    public GameObject blueWinEng;
    public GameObject blueWinIdn;
    public GameObject blueWinJvn;

    public GameObject drawEng;
    public GameObject drawIdn;
    public GameObject drawJvn;

    public GameObject redSFXEng;
    public GameObject redSFXIdn;
    public GameObject redSFXJvn;

    public GameObject blueSFXEng;
    public GameObject blueSFXIdn;
    public GameObject blueSFXJvn;

    public GameObject drawSFXEng;
    public GameObject drawSFXIdn;
    public GameObject drawSFXJvn;

    public rondeMerah merah;
    public rondeBiru biru;
    public bgScriptEv bg;
    public receiveVar var;

    // Update is called once per frame
    void Update()
    {
        if (bg.ronde == 8 && var.language == 1)
        {
            if (merah.scoreMerah < biru.scoreBiru)
            {
                blueWinEng.SetActive(true);
                blueWinIdn.SetActive(false);
                blueWinJvn.SetActive(false);
                redWinEng.SetActive(false);
                redWinIdn.SetActive(false);
                redWinJvn.SetActive(false);
                drawEng.SetActive(false);
                drawIdn.SetActive(false);
                drawJvn.SetActive(false);

                if (var.sfxRecv == 1)
                {
                    blueSFXEng.SetActive(true);
                    blueSFXIdn.SetActive(false);
                    blueSFXJvn.SetActive(false);
                    redSFXEng.SetActive(false);
                    redSFXIdn.SetActive(false);
                    redSFXJvn.SetActive(false);
                    drawSFXEng.SetActive(false);
                    drawSFXIdn.SetActive(false);
                    drawSFXJvn.SetActive(false);
                }
            }
            else if (merah.scoreMerah > biru.scoreBiru)
            {
                blueWinEng.SetActive(false);
                blueWinIdn.SetActive(false);
                blueWinJvn.SetActive(false);
                redWinEng.SetActive(true);
                redWinIdn.SetActive(false);
                redWinJvn.SetActive(false);
                drawEng.SetActive(false);
                drawIdn.SetActive(false);
                drawJvn.SetActive(false);

                if (var.sfxRecv == 1)
                {
                    blueSFXEng.SetActive(false);
                    blueSFXIdn.SetActive(false);
                    blueSFXJvn.SetActive(false);
                    redSFXEng.SetActive(true);
                    redSFXIdn.SetActive(false);
                    redSFXJvn.SetActive(false);
                    drawSFXEng.SetActive(false);
                    drawSFXIdn.SetActive(false);
                    drawSFXJvn.SetActive(false);
                }
            }
            else if (merah.scoreMerah == biru.scoreBiru)
            {
                blueWinEng.SetActive(false);
                blueWinIdn.SetActive(false);
                blueWinJvn.SetActive(false);
                redWinEng.SetActive(false);
                redWinIdn.SetActive(false);
                redWinJvn.SetActive(false);
                drawEng.SetActive(true);
                drawIdn.SetActive(false);
                drawJvn.SetActive(false);

                if (var.sfxRecv == 1)
                {
                    blueSFXEng.SetActive(false);
                    blueSFXIdn.SetActive(false);
                    blueSFXJvn.SetActive(false);
                    redSFXEng.SetActive(false);
                    redSFXIdn.SetActive(false);
                    redSFXJvn.SetActive(false);
                    drawSFXEng.SetActive(true);
                    drawSFXIdn.SetActive(false);
                    drawSFXJvn.SetActive(false);
                }
            }
        }
        else if (bg.ronde == 8 && var.language == 2)
        {
            if (merah.scoreMerah < biru.scoreBiru)
            {
                blueWinEng.SetActive(false);
                blueWinIdn.SetActive(true);
                blueWinJvn.SetActive(false);
                redWinEng.SetActive(false);
                redWinIdn.SetActive(false);
                redWinJvn.SetActive(false);
                drawEng.SetActive(false);
                drawIdn.SetActive(false);
                drawJvn.SetActive(false);

                if (var.sfxRecv == 1)
                {
                    blueSFXEng.SetActive(false);
                    blueSFXIdn.SetActive(true);
                    blueSFXJvn.SetActive(false);
                    redSFXEng.SetActive(false);
                    redSFXIdn.SetActive(false);
                    redSFXJvn.SetActive(false);
                    drawSFXEng.SetActive(false);
                    drawSFXIdn.SetActive(false);
                    drawSFXJvn.SetActive(false);
                }
            }
            else if (merah.scoreMerah > biru.scoreBiru)
            {
                blueWinEng.SetActive(false);
                blueWinIdn.SetActive(false);
                blueWinJvn.SetActive(false);
                redWinEng.SetActive(false);
                redWinIdn.SetActive(true);
                redWinJvn.SetActive(false);
                drawEng.SetActive(false);
                drawIdn.SetActive(false);
                drawJvn.SetActive(false);

                if (var.sfxRecv == 1)
                {
                    blueSFXEng.SetActive(false);
                    blueSFXIdn.SetActive(false);
                    blueSFXJvn.SetActive(false);
                    redSFXEng.SetActive(false);
                    redSFXIdn.SetActive(true);
                    redSFXJvn.SetActive(false);
                    drawSFXEng.SetActive(false);
                    drawSFXIdn.SetActive(false);
                    drawSFXJvn.SetActive(false);
                }
            }
            else if (merah.scoreMerah == biru.scoreBiru)
            {
                blueWinEng.SetActive(false);
                blueWinIdn.SetActive(false);
                blueWinJvn.SetActive(false);
                redWinEng.SetActive(false);
                redWinIdn.SetActive(false);
                redWinJvn.SetActive(false);
                drawEng.SetActive(false);
                drawIdn.SetActive(true);
                drawJvn.SetActive(false);

                if (var.sfxRecv == 1)
                {
                    blueSFXEng.SetActive(false);
                    blueSFXIdn.SetActive(false);
                    blueSFXJvn.SetActive(false);
                    redSFXEng.SetActive(false);
                    redSFXIdn.SetActive(false);
                    redSFXJvn.SetActive(false);
                    drawSFXEng.SetActive(false);
                    drawSFXIdn.SetActive(true);
                    drawSFXJvn.SetActive(false);
                }
            }
        }
        else if (bg.ronde == 8 && var.language == 3)
        {
            if (merah.scoreMerah < biru.scoreBiru)
            {
                blueWinEng.SetActive(false);
                blueWinIdn.SetActive(false);
                blueWinJvn.SetActive(true);
                redWinEng.SetActive(false);
                redWinIdn.SetActive(false);
                redWinJvn.SetActive(false);
                drawEng.SetActive(false);
                drawIdn.SetActive(false);
                drawJvn.SetActive(false);

                if (var.sfxRecv == 1)
                {
                    blueSFXEng.SetActive(false);
                    blueSFXIdn.SetActive(false);
                    blueSFXJvn.SetActive(true);
                    redSFXEng.SetActive(false);
                    redSFXIdn.SetActive(false);
                    redSFXJvn.SetActive(false);
                    drawSFXEng.SetActive(false);
                    drawSFXIdn.SetActive(false);
                    drawSFXJvn.SetActive(false);
                }
            }
            else if (merah.scoreMerah > biru.scoreBiru)
            {
                blueWinEng.SetActive(false);
                blueWinIdn.SetActive(false);
                blueWinJvn.SetActive(false);
                redWinEng.SetActive(false);
                redWinIdn.SetActive(false);
                redWinJvn.SetActive(true);
                drawEng.SetActive(false);
                drawIdn.SetActive(false);
                drawJvn.SetActive(false);

                if (var.sfxRecv == 1)
                {
                    blueSFXEng.SetActive(false);
                    blueSFXIdn.SetActive(false);
                    blueSFXJvn.SetActive(false);
                    redSFXEng.SetActive(false);
                    redSFXIdn.SetActive(false);
                    redSFXJvn.SetActive(true);
                    drawSFXEng.SetActive(false);
                    drawSFXIdn.SetActive(false);
                    drawSFXJvn.SetActive(false);
                }
            }
            else if (merah.scoreMerah == biru.scoreBiru)
            {
                blueWinEng.SetActive(false);
                blueWinIdn.SetActive(false);
                blueWinJvn.SetActive(false);
                redWinEng.SetActive(false);
                redWinIdn.SetActive(false);
                redWinJvn.SetActive(false);
                drawEng.SetActive(false);
                drawIdn.SetActive(false);
                drawJvn.SetActive(true);

                if (var.sfxRecv == 1)
                {
                    blueSFXEng.SetActive(true);
                    blueSFXIdn.SetActive(false);
                    blueSFXJvn.SetActive(false);
                    redSFXEng.SetActive(false);
                    redSFXIdn.SetActive(false);
                    redSFXJvn.SetActive(false);
                    drawSFXEng.SetActive(false);
                    drawSFXIdn.SetActive(false);
                    drawSFXJvn.SetActive(true);
                }
            }
        }
    }
}
